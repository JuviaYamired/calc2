import java.util.*;

public class Calc{
	Map<String,Operation> operations;
	public Calc(){
		operations = new HashMap<String, Operation>();
		operations.put("+", new Sum());
		operations.put("-", new Substraction());
	}
	
	public void register(String operator, Operation operation){
		operations.put(operator,operation);
	}

	public int operate(String operator,int a, int b){
		Operation operation = operations.get(operator);
		return operation.operate(a,b);
	}
}


abstract class Operation {
	public abstract int operate(int a,int b);
}

class Sum extends Operation {
	public int operate(int a, int b){
		return a + b;	 
	}
}

class Substraction extends Operation {
	public int operate(int a,int b){
		return a - b;
	}
}

class Division extends Operation{
	public int operate(int a, int b){
		return a / b;
	}
}

class Multiplicacion extends Operation{
	public int operate(int a, int b){
		return a * b;
	}
}

class Funcion extends Operation{
	public int operate(int a, int b){
		return (b*(a-2))/a+b;
	}
}